import javari.reader.*;
import javari.animal.*;
import javari.park.*;
import javari.writer.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class A3Festival {

    public static void main(String[] args) {

        List<Animal> animals;
        List<Attraction> attractions;
        List<Section> sections;

        Scanner s = new Scanner(System.in);

        System.out.print("Please provide the source data path: ");

        String defaultPath = s.nextLine();

        //Check existence of data files
        File folder = new File(defaultPath);
        Path destination = Paths.get(defaultPath);
        String attractionsFileName = new String();
        String categoriesFileName = new String();
        String recordsFileName = new String();

        try {
            //Assemble data filename
            for (File fileEntry : Objects.requireNonNull(folder.listFiles())) {
                if (fileEntry.getName().contains("animals_attractions.csv")) {
                    attractionsFileName = defaultPath + "/" + fileEntry.getName();
                } else if (fileEntry.getName().contains("animals_categories.csv")) {
                    categoriesFileName = defaultPath + "/" + fileEntry.getName();
                } else if (fileEntry.getName().contains("animals_records.csv")) {
                    recordsFileName = defaultPath + "/" + fileEntry.getName();
                }
            }
            //Assemble data path
            Path attractionsFilePath = Paths.get(attractionsFileName);
            Path categoriesFilePath = Paths.get(categoriesFileName);
            Path recordsFilePath = Paths.get(recordsFileName);

            //Initialize CSV readers
            CsvAttractions attractionReader = new CsvAttractions(attractionsFilePath);
            CsvSections sectionReader = new CsvSections(categoriesFilePath);
            CsvCategories categoriesReader = new CsvCategories(categoriesFilePath);
            CsvRecords recordsReader = new CsvRecords(recordsFilePath);

            //For extra flavor
            System.out.println("... Loading... Success... System is populating data...\n");

            //Count records in each data file
            countRecords(attractionReader, sectionReader, categoriesReader, recordsReader);

            //Initialize all lists pertaining to the festival
            sections = initializeSections(sectionReader, new ArrayList<Section>());
            animals = initializeAnimals(recordsReader, new ArrayList<Animal>());
            attractions = initializeAttractions(attractionReader, animals);

            // ------ Process user input -------

            processUserInput(s, sections, animals, attractions);

        } catch (IOException | NullPointerException e) {
            System.out.println("Error loading data");
        }



        s.close();
    }

    static void countRecords(CsvAttractions attractionReader, CsvSections sectionReader, CsvCategories categoriesReader,
                             CsvRecords recordsReader) {
        System.out.println(String.format("Found %d valid sections and %d invalid sections",
                sectionReader.countValidRecords(), sectionReader.countInvalidRecords()));

        System.out.println(String.format("Found %d valid attractions and %d invalid attractions",
                attractionReader.countValidRecords(), attractionReader.countInvalidRecords()));

        System.out.println(String.format("Found %d valid categories and %d invalid categories",
                categoriesReader.countValidRecords(), categoriesReader.countInvalidRecords()));

        System.out.println(String.format("Found %d valid records and %d invalid records",
                recordsReader.countValidRecords(), recordsReader.countInvalidRecords()));

    }

    static List<Animal> initializeAnimals(CsvRecords recordReader, List<Animal> outputList) {
        try {
            for (String line : recordReader.getLines()) {
                outputList.add(recordReader.initializeAnimal(line));
            }
            System.out.println("Animals successfully initialized");
            return outputList;
        } catch (Exception e) {
            System.out.println("Error on animal creation");
            return null;
        }
    }

    static Attraction initializeAttraction(String name, String[] animalTypes, List<Animal> animals) {

        //Get performers
        ArrayList<Animal> performers = new ArrayList<>();

        for (String type : animalTypes) {
            for (Animal a : animals) {
                if (a.getType().equalsIgnoreCase(type)) {
                    performers.add(a);
                }
            }
        }

        //Initialize attraction
        return new Attraction(name, animalTypes, performers);
    }

    static ArrayList<Attraction> initializeAttractions(CsvAttractions attractionsReader, List<Animal> animals) {
        try {
            ArrayList<Attraction> output = new ArrayList<Attraction>();

            for (String name : attractionsReader.getAttractionNames()) {
                output.add(initializeAttraction(name, attractionsReader.getAnimalTypes(name), animals));
            }
            System.out.println("Attractions successfully initialized");
            return output;
        } catch (Exception e) {
            System.out.println("Error while initializing attractions");
            return null;
        }
    }

    static List<Section> initializeSections(CsvSections sectionReader, List<Section> output) {

        for (String line : sectionReader.getLines()) {
            String query = sectionReader.getCleanLine(line)[2];
            if (findSection(output, query) == null) {
                output.add(new Section(query, sectionReader.getAnimalCategories(query)));
            }
        }

        System.out.println("Sections successfully initialized");
        return output;
    }

    static void processUserInput(Scanner input, List<Section> sections, List<Animal> animals,
                                 List<Attraction> attractions) {
        System.out.println(String.format("Javari Park has %d sections: ", sections.size()));
        for (int i = 1; i <= sections.size(); i++) {
            System.out.println(i + ". " + sections.get(i-1).getName());
        }
        System.out.print("Please choose your preferred section (type number): ");
        String sectionSelection = input.next();

        switch (sectionSelection) {
            case "1":
                selectSection(sections, sections.get(0).getName(), input);
                break;
            case "2":
                selectSection(sections, sections.get(1).getName(), input);
                break;
            case "3":
                selectSection(sections, sections.get(2).getName(), input);
                break;
            default:
                System.out.println("Unknown input");
        }

    }

    static void processSection(Section section) {
        for (int i = 0; i < section.getAnimalCategories().size(); i++) {
            String cur = section.getAnimalCategories().get(i);
            System.out.println(String.format("%d. %s", i+1, cur));
        }
    }

    static void selectSection(List<Section> sections, String sectionName, Scanner input) {
        try {
            Section sect = findSection(sections, sectionName);

            System.out.println(String.format("-- %s --", sect.getName()));

        } catch (NullPointerException e) {
            System.out.println("No such section");
        }
    }

    static Section findSection(List<Section> sections, String name) {
        for (Section s : sections) {
            if (s.getName().equals(name)) {
                return s;
            }
        }
        return null;
    }


}

