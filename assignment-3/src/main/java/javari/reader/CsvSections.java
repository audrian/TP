package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CsvSections extends CsvReader {

    protected List<String> validSections = Arrays.asList("Explore the Mammals", "World of Aves", "Reptillian Kingdom");
    protected List<String> queried = new ArrayList<String>();

    public CsvSections(Path pathToFile) throws IOException {
        super(pathToFile);
    }

    public long countValidRecords() {
        int amount = 0;

        for (String line : this.getLines()) {

            String query = line.split(COMMA)[2].replace("\"", "");

            if (this.validSections.contains(query) && !this.queried.contains(query)) {
                this.queried.add(query);
                amount++;
            }
        }

        return amount;
    }

    public long countInvalidRecords() {
        int amount = 0;

        for (String line : this.lines) {
            String query = line.split(COMMA)[2].replace("\"", "");
            if (!this.validSections.contains(query)) {
                amount++;
            }
        }

        return amount;
    }

    public ArrayList<String> getAnimalCategories(String sectionName) {
        ArrayList<String> output = new ArrayList<String>();

        for (String line : this.lines) {
            String[] cleanLine = this.getCleanLine(line);
            String query = cleanLine[1];
            if (!output.contains(query) && cleanLine[2].equals(sectionName)) {
                output.add(query);
            }
        }

        return output;
    }
}