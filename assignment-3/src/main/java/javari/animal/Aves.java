package javari.animal;

public class Aves extends Animal {

    protected boolean isLayingEggs;

    public Aves(Integer id, String type, String name, double length, double weight, Gender gender,
                String specificCondition, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.isLayingEggs = specificCondition.equalsIgnoreCase("laying eggs");
    }

    public boolean specificCondition() {
        return !this.isLayingEggs;
    }
}