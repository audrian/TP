package animal;
import java.util.Scanner;
public class Parrot extends Animal {

    public Parrot(String name, int length) {
        super(name, length);
        this.outdoor = false;
        this.species = "Parrot";
    }

    public action() {
        Scanner inp = new Scanner(System.in);
        int cmd = Integer.parseInt(inp.nextLine());

        if (cmd == 1) fly();
        else if (cmd == 2) {
            System.out.print("You say: ");
            String phrase = inp.nextLine();
            speak(phrase);
        } else {
            System.out.println(this.getName() + " says: HM?");
        }
    }

    public void fly() {
        System.out.println("Parrot " + this.getName() + " flies!");
        System.out.println(this.getName() + " makes a voice: FLYYYY.....");
    }

    public void speak(String phrase) {
        System.out.println(this.getName() + " says: " + phrase.toUpperCase());
    }
}