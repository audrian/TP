package animal;
public class Animal {

    protected String name;
    protected int length;
    protected boolean outdoor;
    protected String species;

    public Animal(String name, int length) {
        this.name = name;
        this.length = length;
    }

    public String getName() {
        return this.name;
    }

    public int getLength() {
        return this.length;
    }

    public boolean isOutdoor() {
        return this.outdoor;
    }

    public String getSpecies() {
        return this.species;
    }
}