package cage;
import animal.*;
import java.util.List;
import java.util.ArrayList;

public class CageArrangement {
    ArrayList<Cage> level1 = new ArrayList<Cage>();
    ArrayList<Cage> level2 = new ArrayList<Cage>();
    ArrayList<Cage> level3 = new ArrayList<Cage>();
    ArrayList<Cage> whole = new ArrayList<Cage>();
    ArrayList<ArrayList<Cage>> allLevels = new ArrayList<ArrayList<Cage>>();

    public CageArrangement(ArrayList<Animal> animals) {
        for (Animal a : animals) {
            whole.add(new Cage(a));
        }
        generateCageArrangement(whole);
    }

    public void generateCageArrangement(ArrayList<Cage> cages) {
        int levelSize = cages.size() / 3;
        int modulo = cages.size() % 3;

        if (!cages.isEmpty()) {
            if (cages.size() < 3) {
                for (Cage c : cages) {
                    level1.add(c);
                }
            } else {
                int end1 = levelSize + modulo;
                int end2 = end1 + levelSize;
                level1 = new ArrayList<Cage>(cages.subList(0, end1));
                level2 = new ArrayList<Cage>(cages.subList(end1, end2));
                level3 = new ArrayList<Cage>(cages.subList(end2, cages.size()-1));
            } 
        }
    }

    public ArrayList<Cage> reverseLevel(ArrayList<Cage> cages) {
        ArrayList<Cage> result = new ArrayList<Cage>();

        for (int i = cages.size()-1; i >= 0; i--) {
            result.add(cages.get(i));
        }

        return result;
    }

    public ArrayList<ArrayList<Cage>> rearrangeCages() {

        ArrayList<Cage> level1r = reverseLevel(level1);
        ArrayList<Cage> level2r = reverseLevel(level2);
        ArrayList<Cage> level3r = reverseLevel(level3);

        ArrayList<Cage> temp1 = level3r;
        ArrayList<Cage> temp3 = level1r;

        this.level1 = temp1;
        this.level2 = level2r;
        this.level3 = temp3;

        return this.allLevels;
    }

    public void printInfo() {
        String location = "indoor";
        if (whole.get(0).getAnimal().isOutdoor()) location = "indoor";

        System.out.print("Location: " + location);
        System.out.println();

        System.out.print("Level 1: ");
        printLevel(this.level1);

        System.out.print("Level 2: ");
        printLevel(this.level2);

        System.out.print("Level 3: ");
        printLevel(this.level3);

        System.out.print("\n\n");
    }

    public int getSize() {
        int result = 0;
        for (ArrayList<Cage> level : this.allLevels) {
            result += level.size();
        }

        return result;
    }

    public void printLevel(ArrayList<Cage> level) {
        for (Cage c : level) {
            System.out.print(c.printInfo());;
        }
        System.out.println();
    }
}