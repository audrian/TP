import java.util.Random;

public class Cat extends Animal {

    public Cat(String name, int length) {
        super(name, length);
        this.outdoor = false;
    }

    public void brush() {
        System.out.printf("Time to brush %s's hair\n", this.getName());
        System.out.printf("%s makes a voice: Nyaaan....\n", this.getName());
    }

    public void cuddle() {
        String[] responses = {"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};
        String voice = responses[new Random().nextInt(3)];

        System.out.printf("%s makes a voice: %s\n", this.getName(), voice);
    }

    public static void main(String[] args) {
        Cat shiro = new Cat("Shiro", 60);
        shiro.brush();
        shiro.cuddle();
    }
}