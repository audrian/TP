package javari.animal;

public class Mammal extends Animal {

    protected boolean isPregnant;

    public Mammal(Integer id, String type, String name, double length, double weight, Gender gender, 
    String specificCondition, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.isPregnant = specificCondition.equalsIgnoreCase("pregnant");
    }

    public boolean specificCondition() {
        if (this.getType().equalsIgnoreCase("Lion")) {
            return this.getGender() == Gender.MALE;
        } else {
            return !this.isPregnant;
        }
    }
}