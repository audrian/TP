package javari.animal;

public class Reptile extends Animal {

    protected boolean isTame;

    public Reptile(Integer id, String type, String name, double length, double weight, Gender gender,
    String specificCondition, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.isTame = specificCondition.equalsIgnoreCase("tame");
    }

    public boolean specificCondition() {
        return this.isTame;
    }
}