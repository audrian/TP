package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class CsvCategories extends CsvReader {

    protected List<String> validCategories = Arrays.asList("mammals", "aves", "reptiles");
    protected List<String> queriedCategories = new ArrayList<String>();

    public CsvCategories(Path pathToFile) throws IOException {
        super(pathToFile);
    }

    public long countValidRecords() {
        int amount = 0;

        for (String line : this.lines) {
            String query = line.split(COMMA)[1].replace("\"", "");
            if (this.validCategories.contains(query) && !this.queriedCategories.contains(query)) {
                this.queriedCategories.add(query);
                amount++;
            }
        }

        return amount;
    }

    public long countInvalidRecords() {
        int amount = 0;

        for (String line : this.lines) {
            String query = line.split(COMMA)[1].replace("\"", "");
            if (!this.validCategories.contains(query)) {
                amount ++;
            }
        }

        return amount;
    }
}