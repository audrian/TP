package javari.reader;

import javari.animal.Animal;
import javari.park.Attraction;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CsvAttractions extends CsvReader {

    private List<String> validAttractions = Arrays.asList("Circles of Fires", "Dancing Animals", "Counting Masters",
            "Passionate Coders");
    private List<String> queriedAttractions = new ArrayList<String>();


    public CsvAttractions(Path pathToFile) throws IOException {
        super(pathToFile);
    }

    public ArrayList<String> getAttractionNames() {

        ArrayList<String> attractionNames = new ArrayList<String>();

        for (String line : this.lines) {
            String query = line.split(",")[1].replace("\"","");
            if (!attractionNames.contains(query)) {
                attractionNames.add(query);
            }
        }

        return attractionNames;
    }

    public long countValidRecords() {
        int amount = 0;

        for (String line : this.lines) {
            String query = line.split(COMMA)[1].replace("\"", "");
            if (this.validAttractions.contains(query) && !this.queriedAttractions.contains(query)) {
                this.queriedAttractions.add(query);
                amount++;
            }
        }

        return amount;
    }

    public long countInvalidRecords() {
        int amount = 0;

        for (String line : this.lines) {
            String query = line.split(COMMA)[1].replace("\"","");
            if (!this.validAttractions.contains(query)) {
                amount++;
            }
        }

        return amount;
    }

    public List<String> getUniqueAttractionNames() {

        List<String> output = new ArrayList<String>();

        for (String line : this.lines) {
            String name = this.getCleanLine(line)[1];
            if (!output.contains(name)) {
                output.add(name);
            }
        }

        return output;
    }

    public List<String> getAttractionAnimalTypes(String attractionName) {

        List<String> output = new ArrayList<String>();

        for (String line : this.lines) {
            String name = this.getCleanLine(line)[0];
            if (this.getCleanLine(line)[1].equals(attractionName) && !output.contains(name)) {
                output.add(name);
            }
        }

        return output;
    }

}