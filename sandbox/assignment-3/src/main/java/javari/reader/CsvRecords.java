package javari.reader;

import javari.animal.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Arrays;

public class CsvRecords extends CsvReader {

    protected List<String> validTypes = Arrays.asList("Hamster", "Lion", "Cat", "Eagle", "Parrot", "Snake", "Whale");
    protected List<String> validConditions = Arrays.asList("healthy", "not healthy");
    protected List<String> validGenders = Arrays.asList("male", "female");
    protected List<String> mammals = Arrays.asList("Hamster", "Lion", "Cat", "Whale");
    protected List<String> aves = Arrays.asList("Eagle", "Parrot");
    protected List<String> reptiles = Arrays.asList("Snake");

    public CsvRecords(Path pathToFile) throws IOException {
        super(pathToFile);
    }

    public long countValidRecords() {
        int amount = 0;

        for (String line : this.lines) {

            if (checkValidity(line)) {
                amount++;
            }
        }

        return amount;
    }

    public long countInvalidRecords() {
        int amount = 0;

        for (String line : this.lines) {
            if (!checkValidity(line)) {
                amount++;
            }
        }

        return amount;
    }

    public Animal initializeAnimal(String input) {
        if (checkValidity(input)) {
            String[] animalData = input.split(",");

            for (int i = 0; i < animalData.length; i++) {
                animalData[i] = animalData[i].replace("\"", "");
            }

            Integer id = Integer.parseInt(animalData[0]);
            String type = animalData[1];
            String name = animalData[2];
            String gender = animalData[3];
            double length = Double.parseDouble(animalData[4]);
            double weight = Double.parseDouble(animalData[5]);
            String specificCondition = animalData[6];
            String condition = animalData[7];

            if (mammals.contains(type)) {
                return new Mammal(id, type, name, length, weight, Gender.parseGender(gender), specificCondition,
                        Condition.parseCondition(condition));
            } else if (aves.contains(type)) {
                return new Aves(id, type, name, length, weight, Gender.parseGender(gender), specificCondition,
                        Condition.parseCondition(condition));
            } else if (reptiles.contains(type)) {
                return new Reptile(id, type, name, length, weight, Gender.parseGender(gender), specificCondition,
                        Condition.parseCondition(condition));
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public boolean checkValidity(String input) {

        String[] str = input.split(",");

        for (int i = 0; i < str.length; i++) {
            str[i] = str[i].replace("\"", "");
        }

        boolean validID = (!str[0].isEmpty());
        boolean validType = this.validTypes.contains(str[1]);
        boolean validName = (!str[2].isEmpty());
        boolean validGender = this.validGenders.contains(str[3]);
        boolean validLength = (!str[4].isEmpty());
        boolean validWeight = (!str[5].isEmpty());
        boolean validCondition = this.validConditions.contains(str[7]);

        return validID && validType && validName && validGender && validLength && validWeight && validCondition;
    }
}