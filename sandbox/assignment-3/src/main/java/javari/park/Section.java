package javari.park;

import java.util.List;

public class Section {

    private List<String> animalCategories;
    private String name;

    public Section(String name, List<String> animals) {
        this.name = name;
        this.animalCategories = animals;
    }

    public String getName() { return this.name; }

    public List<String> getAnimalTypes() { return this.animalCategories; }
}
