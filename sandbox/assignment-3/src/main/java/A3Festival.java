import javari.reader.*;
import javari.animal.*;
import javari.park.*;
import javari.writer.*;
import org.w3c.dom.Attr;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class A3Festival {

    private static List<Animal> animals;
    private static List<Attraction> attractions;
    private static List<Section> sections;

    public static void main(String[] args) {

        Scanner s = new Scanner(System.in);

        System.out.print("Please provide the source data path: ");

        String defaultPath = s.nextLine();

        //Check existence of data files
        File folder = new File(defaultPath);
        Path destination = Paths.get(defaultPath);
        String attractionsFileName = new String();
        String categoriesFileName = new String();
        String recordsFileName = new String();

        try {
            //Assemble data filename
            for (File fileEntry : Objects.requireNonNull(folder.listFiles())) {
                if (fileEntry.getName().contains("animals_attractions.csv")) {
                    attractionsFileName = defaultPath + "/" + fileEntry.getName();
                } else if (fileEntry.getName().contains("animals_categories.csv")) {
                    categoriesFileName = defaultPath + "/" + fileEntry.getName();
                } else if (fileEntry.getName().contains("animals_records.csv")) {
                    recordsFileName = defaultPath + "/" + fileEntry.getName();
                }
            }
            //Assemble data path
            Path attractionsFilePath = Paths.get(attractionsFileName);
            Path categoriesFilePath = Paths.get(categoriesFileName);
            Path recordsFilePath = Paths.get(recordsFileName);

            //Initialize CSV readers
            CsvAttractions attractionReader = new CsvAttractions(attractionsFilePath);
            CsvSections sectionReader = new CsvSections(categoriesFilePath);
            CsvCategories categoriesReader = new CsvCategories(categoriesFilePath);
            CsvRecords recordsReader = new CsvRecords(recordsFilePath);

            //For extra flavor
            System.out.println("... Loading... Success... System is populating data...\n");

            //Count records in each data file
            countRecords(attractionReader, sectionReader, categoriesReader, recordsReader);

            //Initialize all lists pertaining to the festival
            sections = initializeSections(sectionReader, new ArrayList<Section>());
            animals = initializeAnimals(recordsReader, new ArrayList<Animal>());
            attractions = initializeAttractions(attractionReader, animals);


            // ------ Process user input -------

            processUserInput(s);

        } catch (IOException | NullPointerException e) {
            System.out.println("Error loading data");
        }

        s.close();
    }

    static void countRecords(CsvAttractions attractionReader, CsvSections sectionReader, CsvCategories categoriesReader,
                             CsvRecords recordsReader) {
        System.out.println(String.format("Found %d valid sections and %d invalid sections",
                sectionReader.countValidRecords(), sectionReader.countInvalidRecords()));

        System.out.println(String.format("Found %d valid attractions and %d invalid attractions",
                attractionReader.countValidRecords(), attractionReader.countInvalidRecords()));

        System.out.println(String.format("Found %d valid categories and %d invalid categories",
                categoriesReader.countValidRecords(), categoriesReader.countInvalidRecords()));

        System.out.println(String.format("Found %d valid records and %d invalid records",
                recordsReader.countValidRecords(), recordsReader.countInvalidRecords()));

    }

    static List<Animal> initializeAnimals(CsvRecords recordReader, List<Animal> outputList) {
        try {
            for (String line : recordReader.getLines()) {
                try {
                    //Initialize animal based on each line
                    outputList.add(recordReader.initializeAnimal(line));
                } catch (Exception e) {
                    //Skip invalid records
                    continue;
                }
            }
            System.out.println("Animals successfully initialized");
            return outputList;
        } catch (Exception e) {
            System.out.println("Error on animal creation");
            return null;
        }
    }

    static void processUserInput(Scanner s) {
        System.out.println("Welcome to Javari Park -- Registration Service!\n");
        System.out.println(String.format("Javari Park has %d sections: ", sections.size()));

        //List all available sections
        for (int i = 1; i <= sections.size(); i++) {
            System.out.println(String.format("%d. %s", i, sections.get(i-1).getName()));
        }

        System.out.print("Please choose your preferred section: ");
        Integer selection = s.nextInt();

        //Select section based on user input
        if (selection >= 1 && selection <= sections.size()) {
            //Call method handling section input
            sectionInput(s, selection-1);
            System.out.println();
        } else {
            System.out.println("Input invalid");
        }
    }

    static void sectionInput(Scanner s, int index) {
        try {
            Section selection = sections.get(index);

            //List all animals in section
            System.out.println(String.format("-- %s --", selection.getName()));
            for (int i = 1; i <= selection.getAnimalTypes().size(); i++) {
                System.out.println(String.format("%d. %s", i, selection.getAnimalTypes().get(i-1)));
            }

            //Handle animal selection
            System.out.print("Please choose your preferred animals: ");
            String input = s.next();



        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static ArrayList<Attraction> initializeAttractions(CsvAttractions attractionsReader, List<Animal> animals) {

        ArrayList<Attraction> output = new ArrayList<>();

        try {
           for (String attName : attractionsReader.getUniqueAttractionNames()) {
               List<String> attTypesList = attractionsReader.getAttractionAnimalTypes(attName);
               String[] attTypes = attTypesList.stream().toArray(String[]::new);
               output.add(new Attraction(attName, attTypes, animals));
           }
           System.out.println("Attractions successfully initialized");

           return output;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error while initializing attractions");
            return null;
        }
    }

    static List<Section> initializeSections(CsvSections sectionReader, List<Section> output) {

        for (String line : sectionReader.getLines()) {
            String query = sectionReader.getCleanLine(line)[2];
            if (findSection(output, query) == null) {
                output.add(initializeSection(query));
            }
        }

        System.out.println("Sections successfully initialized");
        return output;
    }

    static Section initializeSection(String sectionName) {
        if (sectionName.equalsIgnoreCase("Explore the Mammals")) {
            return new Section("Explore the Mammals", Arrays.asList("Hamster", "Lion", "Cat", "Whale"));
        } else if (sectionName.equalsIgnoreCase("World of Aves")) {
            return new Section("World of Aves", Arrays.asList("Eagle", "Parrot"));
        } else if (sectionName.equalsIgnoreCase("Reptillian Kingdom")) {
            return new Section("Reptillian Kingdom", Arrays.asList("Snake"));
        } else {
            return null;
        }
    }

    static Section findSection(List<Section> sections, String name) {
        for (Section s : sections) {
            if (s.getName().equalsIgnoreCase(name)) {
                return s;
            }
        }
        return null;
    }

    static int getShowableAnimals(List<Animal> animals, String type) {
        int amount = 0;
        for (Animal a : animals) {
            if (a.getType().equalsIgnoreCase(type) && a.isShowable()) {
                amount++;
            }
        }

        return amount;
    }

}

