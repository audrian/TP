import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms

    // You can add new variables or methods in this class

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        TrainCar recent = null;

        // add the train cars
        int amountOfCars = Integer.parseInt(input.nextLine());
        for (int i = 0; i < amountOfCars; i++) {
          String[] command = input.nextLine().split(",");
          WildCat newCat = new WildCat(command[0], Double.parseDouble(command[1]), Double.parseDouble(command[2]));
          TrainCar newCar = new TrainCar(newCat);
          if (recent == null) {
            recent = newCar;
          } else {
            newCar.next = recent;
            recent = newCar;
          }

          //Check total weight of all train cars
          if (recent.computeTotalWeight() > 250) {
            depart(recent);
          }

        }
        //Depart the train anyway
        depart(recent);
    }

    public static void depart(TrainCar recent) {
      double totalMassIndex = recent.computeTotalMassIndex();
      String massCategory = "";
      System.out.println("The train departs to Javari Park");
      System.out.print("[LOCO]--");
      recent.printCar();
      System.out.println("Average mass index of all cats: " + String.format("%.2f", totalMassIndex));
      if (totalMassIndex < 18.5) massCategory = "underweight";
      else if (totalMassIndex >= 18.5 && totalMassIndex < 25) massCategory = "normal";
      else if (totalMassIndex >= 25 && totalMassIndex < 30) massCategory = "overweight";
      else if (totalMassIndex >= 30) massCategory = "overweight";
    }
}
