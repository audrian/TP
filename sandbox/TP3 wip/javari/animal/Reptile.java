package javari.animal;

public class Reptile extends Animal {

    protected boolean isTame;

    public Reptile(Integer id, String type, String name, double length, double weight, Gender gender,
    boolean isTame, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.isTame = isTame;
    }

    public boolean specificCondition() {
        return this.isTame;
    }
}