package javari.reader;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Arrays;

public class CsvRecords extends CsvReader {

    protected List<String> validTypes = Arrays.asList("Hamster", "Lion", "Cat", "Eagle", "Parrot", "Snake", "Whale");
    protected List<String> validConditions = Arrays.asList("healthy", "not healthy");
    protected List<String> validGenders = Arrays.asList("male", "female");

    public CsvRecords(Path pathToFile) throws IOException {
        super(pathToFile);
    }

    public long countValidRecords() {
        int amount = 0;

        for (String line : this.lines) {
            String[] splitLine = line.split(COMMA);
            if (checkValidity(splitLine)) {
                amount++;
            }
        }

        return amount;
    }

    public long countInvalidRecords() {
        int amount = 0;

        for (String line : this.lines) {
            String[] splitLine = line.split(COMMA);
            if (!checkValidity(splitLine)) {
                amount++;
            }
        }

        return amount;
    }

    public boolean checkValidity(String[] str) {

        boolean validID = (!str[0].isEmpty());
        boolean validType = this.validTypes.contains(str[1]);
        boolean validName = (!str[2].isEmpty());
        boolean validGender = this.validGenders.contains(str[3]);
        boolean validLength = (!str[4].isEmpty());
        boolean validWeight = (!str[5].isEmpty());
        boolean validCondition = this.validConditions.contains(str[7]);

        return validID && validType && validName && validGender && validLength && validWeight && validCondition;
    }
}