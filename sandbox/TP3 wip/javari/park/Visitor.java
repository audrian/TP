package javari.park;

import java.util.List;
import javari.animal.*;

public class Visitor implements Registration {

    private int id;
    private String name;
    private List<SelectedAttraction> selectedAttractions;

    public Visitor(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getVisitorName() { return this.name; }

    public String setVisitorName(String name) {
        this.name = name;
        return this.name;
    }

    public int getRegistrationId() { return this.id; }

    public List<SelectedAttraction> getSelectedAttractions() { return this.selectedAttractions; }

    public boolean addSelectedAttraction(SelectedAttraction selected) {
        this.selectedAttractions.add(selected);
    }
}