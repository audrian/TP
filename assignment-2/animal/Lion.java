package animal;
import java.util.Scanner;
public class Lion extends Animal {

    public Lion(String name, int length) {
        super(name, length);
        this.outdoor = true;
        this.species = "Lion";
    }

    public action() {
        Scanner inp = new Scanner(System.in);
        int cmd = Integer.parseInt(inp.nextLine());

        if (cmd == 1) hunt();
        else if (cmd == 2) brush();
        else if (cmd == 3) disturb();
        else System.out.println("You do nothing...");
    }

    public void hunt() {
        System.out.println("Lion is hunting.");
        System.out.println(this.getName() + " makes a voice: err...");
    }

    public void brush() {
        System.out.println("Clean the lion's mane...");
        System.out.println(this.getName() + " makes a voice: Hauhhmm!");
    }

    public void disturb() {
        System.out.println(this.getName() + " makes a voice: HAUHHMM!");
    }
}