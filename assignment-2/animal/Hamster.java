package animal;
import java.util.Scanner;
public class Hamster extends Animal {

    public Hamster(String name, int length) {
        super(name, length);
        this.outdoor = false;
        this.species = "Hamster";
    }

    public action() {
        Scanner inp = new Scanner(System.in);
        int cmd = Integer.parseInt(inp.nextLine());

        if (cmd == 1) gnaw();
        else if (cmd == 2) run();
        else System.out.println("You do nothing...");
    }

    public void gnaw() {
        System.out.println(this.getName() + " makes a voice: ngkkrit.. ngkkrrriiit");
    }

    public void run() {
        System.out.println(this.getName() + " makes a voice: trrr.... trrr....");
    }
}