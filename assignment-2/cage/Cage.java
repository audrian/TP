package cage;
import animal.*;
public class Cage {

    protected Animal animal;
    protected String cageType;

    public Cage(Animal animal) {
        this.animal = animal;
        this.cageType = this.setCageType(this.animal);
    }

    public Animal getAnimal() {
        return this.animal;
    }

    public String getAnimalName() {
        return this.animal.getName();
    }

    public String getAnimalSpecies() {
        return this.animal.getSpecies();
    }

    public int getAnimalLength() {
        return this.animal.getLength();
    }

    public String getCageType() {
        return this.cageType;
    }

    public String setCageType(Animal animal) {
        if (animal.isOutdoor()) {
            if (animal.getLength() < 75) { 
                return "A";
            } else if (animal.getLength() >= 75 && animal.getLength() > 90) {
                return "B";
            } else {
                return "C";
            }
        } else {
            if (animal.getLength() < 45) {
                return "A";
            } else if (animal.getLength() >= 45 && animal.getLength() <= 60) {
                return "B";
            } else {
                return "C";
            }
        }
    }

    public String printInfo() {
        String out = this.getAnimalName() + " (" + 
                     this.getAnimalLength() + " - " + 
                     this.getCageType() + "), ";
        return out;
    }
}