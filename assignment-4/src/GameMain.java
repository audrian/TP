public class GameMain {

    private static GameBoard game;

    static void reset() {
        game = new GameBoard();
    }

    public static void main(String[] args) {
        game = new GameBoard();
    }
}