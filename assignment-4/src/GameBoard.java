import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.*;
import javax.swing.*;
import javax.imageio.*;
import java.util.ArrayList;
import java.util.Collections;
import java.io.File;
import java.io.IOException;

public class GameBoard {

    private static ArrayList<Card> cards;

    private static Card selected1 = null;
    private static Card selected2 = null;

    private static int attempts = 0;
    private static JFrame frame;
    private static JPanel boardPanel;
    private static JPanel statusBar;

    public GameBoard() {
        initGameWindow();
        initGameBoard();
        initCards();
        initStatusBar();
        frame.pack();
        frame.setVisible(true);
    }

    static void initStatusBar() {
        //Initialize status bar
        statusBar = new JPanel();
        statusBar.setLayout(new GridLayout(1, 3));
        statusBar.setPreferredSize(new Dimension(frame.getWidth(), 30));

        //Create reset button
        JButton resetButton = new JButton("Reset game");
        resetButton.addActionListener(e -> resetGame());
        statusBar.add(resetButton);

        frame.getContentPane().add(statusBar, BorderLayout.SOUTH);
    }

    static void initGameWindow() {
        frame = new JFrame("Matcha - Tries: 0");
        frame.setLayout(new BorderLayout());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    static void initGameBoard() {
        boardPanel = new JPanel();
        boardPanel.setLayout(new GridLayout(6, 6));
        frame.getContentPane().add(boardPanel, BorderLayout.CENTER);
    }

    static void initCards() {
        //Wipe arraylist of cards clean
        cards = new ArrayList<>();

        int typeCounter = 1; //used to determine type of card
        for (int i = 0; i < 36; i++) {
            try {
                //process image
                Image source = ImageIO.read(new File("icons/" + typeCounter + ".png"));
                source = source.getScaledInstance(60, 60, Image.SCALE_SMOOTH);

                //initialize card
                Card temp = new Card(typeCounter, new ImageIcon("logonew.png"),
                        new ImageIcon(source));
                typeCounter++;
                temp.addActionListener(e -> checkPress(temp));
                cards.add(temp);
                //Ensure all cards have pairs
                if (typeCounter > 18) typeCounter = 1;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Collections.shuffle(cards);

        for (Card c : cards) {
            boardPanel.add(c);
        }
    }

    static void resetGame() {
        GameMain.reset();
    }

    static boolean checkWin() {
        for (Card c : cards) {
            if (!c.isMatched()) {
                return false;
            }
        }
        return true;
    }

    static void win() {
        if (checkWin()) {
            JOptionPane.showMessageDialog(null,
                    String.format("Congratulations, you won in %d tries", attempts));
        }
    }

    static void checkPress(Card c) {
        c.setEnabled(false);
        if (selected1 == null && selected2 == null) {
            selected1 = c;
        } else if (selected1 != null && selected2 == null) {
            selected2 = c;
            attempts++;
            frame.setTitle("Matcha - Tries: " + attempts);
            if (selected1.matches(selected2)) {
                //'delete' the matching cards
                selected1.setVisible(false);
                selected2.setVisible(false);

                //Set matched status, useful for checking wins
                selected1.setMatched(true);
                selected2.setMatched(true);

                //Check winning status
                if (checkWin()) {
                    win();
                }
            }
        } else if (selected1 != null && selected2 != null) {
            selected1.setEnabled(true);
            selected2.setEnabled(true);
            selected1 = c;
            selected2 = null;
        }
    }
}
